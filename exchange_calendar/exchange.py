from collections.abc import Iterator
from datetime import date, datetime, timedelta

from exchangelib import IMPERSONATION, UTC, Account, CalendarItem, Credentials


def daterange(start_date: date, end_date: date) -> Iterator[date]:
    days = int((end_date - start_date).days) + 1
    for n in range(days):
        yield start_date + timedelta(days=n)


def get_events(credentials: Credentials, accounts: dict[str, str], days: int, _date: date) -> dict[str, list[CalendarItem]]:
    start = datetime(_date.year, _date.month, _date.day, tzinfo=UTC)
    end = start + timedelta(days=days)

    events = {}
    for alias, mail in accounts.items():
        account = Account(primary_smtp_address=mail, credentials=credentials, autodiscover=True, access_type=IMPERSONATION)
        events_timespan = []
        for event in account.calendar.view(start=start, end=end):
            if event.is_all_day:
                for date_in_event_range in daterange(event.start, event.end):
                    if start.date() == date_in_event_range:
                        events_timespan.append(event)
                        break
            else:
                events_timespan.append(event)
        events[alias] = events_timespan

    return events
