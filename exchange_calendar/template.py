from collections.abc import Callable
from pathlib import Path

from babel.dates import format_time
from jinja2 import BaseLoader, Environment, Template, TemplateNotFound, select_autoescape
from maubot.loader import BasePluginLoader
from mautrix.util import markdown


class PluginTemplateLoader(BaseLoader):
    plugin_loader: BasePluginLoader
    directory: Path

    def __init__(self, loader: BasePluginLoader, directory: Path) -> None:
        self.plugin_loader = loader
        self.directory = directory

    def get_source(self, environment: Environment, name: str) -> tuple[str, str, Callable[[], bool]]:
        path = self.directory / f'{name}.j2'
        try:
            tpl = self.plugin_loader.sync_read_file(str(path))
        except KeyError as err:
            raise TemplateNotFound(name) from err
        return tpl.decode('utf-8'), name, lambda: True


class TemplateManager:
    _env: Environment
    _loader: PluginTemplateLoader

    def __init__(self, loader: BasePluginLoader, directory: Path, locale: str) -> None:
        self._loader = PluginTemplateLoader(loader, directory)
        self._env = Environment(loader=self._loader, trim_blocks=True, autoescape=select_autoescape())
        self._env.filters['markdown'] = lambda message: markdown.render(message, allow_html=False)
        self._env.filters['format_time'] = lambda datetime_obj: format_time(datetime_obj, locale=locale, format='short')

    def __getitem__(self, item: str) -> Template:
        return self._env.get_template(item)
