import asyncio
from collections.abc import Iterator
from datetime import date, datetime, timedelta
from pathlib import Path

from cron_converter import Cron
from exchangelib import CalendarItem, Credentials
from maubot import MessageEvent, Plugin
from maubot.handlers import command
from maubot.matrix import parse_formatted
from mautrix.types import EventID, Format, MessageType, RoomID, TextMessageEventContent, UserID
from mautrix.util import background_task
from mautrix.util.config import BaseProxyConfig, ConfigUpdateHelper

from . import messages
from .exchange import get_events
from .template import TemplateManager


def _parse_date(string: str) -> date | None:
    if string == '':
        return None
    return datetime.strptime(string, '%Y-%m-%d').date()


class Config(BaseProxyConfig):
    def do_update(self, helper: ConfigUpdateHelper) -> None:
        helper.copy('management_rooms')
        helper.copy('allowlist')
        helper.copy('calendars')
        helper.copy('exchange')
        helper.copy('locale')
        helper.copy('notifications')


class ExchangeCalendarBot(Plugin):
    templates: TemplateManager
    notification_loop_task: asyncio.Future

    @classmethod
    def get_config_class(cls) -> type[BaseProxyConfig]:
        return Config

    async def start(self) -> None:
        await super().start()
        self.config.load_and_update()
        self.templates = TemplateManager(self.loader, Path('templates'), self.config['locale'])
        self.notification_loop_task = asyncio.create_task(self.notification_loop())

    async def stop(self) -> None:
        self.notification_loop_task.cancel()

    async def notification_loop(self) -> None:
        try:
            self.log.debug('Notification loop started')
            while True:
                now = datetime.now()
                next_minute = (now + timedelta(minutes=1)).replace(second=0, microsecond=0)
                await asyncio.sleep((next_minute - now).total_seconds())
                await self.schedule_nearby_notifications(next_minute)
        except asyncio.CancelledError:
            self.log.debug('Notification loop stopped')
        except Exception:
            self.log.exception('Exception in Notification loop')

    async def schedule_nearby_notifications(self, now: datetime) -> None:
        until = now + timedelta(minutes=1)
        self.log.debug('Checking for notifications from %s until %s', now, until)
        for notification_settings, next_occurence in self.get_notifications(now, until):
            self.log.debug('Scheduling notification for %s at %s', notification_settings['room'], next_occurence)
            background_task.create(
                self.send_notification(notification_settings=notification_settings, notification_time=next_occurence)
            )

    def get_notifications(self, before: datetime, after: datetime) -> Iterator[tuple[dict[str, str | int | bool], datetime]]:
        notifications = self.config.get('notifications', [])

        for notification in notifications:
            try:
                cron = Cron(notification['cron'])
            except ValueError:
                self.log.warning('Got no valid cron syntax "%s". Ignoring this entry.', notification['cron'])
                continue
            next_occurrence = cron.schedule(before).next()

            if before <= next_occurrence < after:
                yield notification, next_occurrence

    async def send_notification(self, notification_settings: dict[str, str | int | bool], notification_time: datetime) -> None:
        try:
            await self._send_notification(notification_settings=notification_settings, notification_time=notification_time)
        except Exception:
            self.log.exception('Error sending notification')

    async def _send_notification(self, notification_settings: dict[str, str | int | bool], notification_time: datetime) -> None:
        wait_time = (notification_time - datetime.now()).total_seconds()

        room_id = RoomID(notification_settings['room'])
        days = int(notification_settings.get('days', 1))
        check_date = _parse_date(notification_settings.get('check_date', ''))
        if days < 1:
            self.log.warning('Days < 1 have weird filter-effects. Setting to 1')
            days = 1
        send_empty = bool(notification_settings.get('send-empty', True))

        if wait_time > 0:
            self.log.debug('Waiting %d seconds for reminder in room %s', wait_time, room_id)
            await asyncio.sleep(wait_time)
        else:
            self.log.debug('Sending notification in room %s immediately', room_id)

        await self._send_message(room_id=room_id, days=days, check_date=check_date, send_empty=send_empty)

    async def _send_message(
        self,
        room_id: RoomID,
        days: int,
        check_date: date | None = None,
        send_empty: bool = True,
        reply_to_event: EventID | None = None
    ) -> None:
        if check_date is None:
            check_date = date.today()
        credentials = Credentials(**self.config['exchange'])
        calendars: dict[str, str] = self.config['calendars']

        exchange_data = get_events(credentials, calendars, days, check_date)
        if not send_empty and not any(exchange_data.values()):
            self.log.debug('No events to send. `send-empty` is set to %s so we are not sending a message.', send_empty)
            return

        message_text = self.generate_message(exchange_data, days, check_date)

        message_plain, message_html = await parse_formatted(message_text, allow_html=False, render_markdown=True)

        content = TextMessageEventContent(
            msgtype=MessageType.NOTICE,
            format=Format.HTML,
            body=message_plain,
            formatted_body=message_html,
        )

        if reply_to_event is not None:
            content.set_reply(reply_to_event)

        await self.client.send_message(room_id, content)

    @staticmethod
    def _sort_events_by_day(calendar_events: dict[str, list[CalendarItem]]) -> dict[date, dict[str, CalendarItem]]:
        events_by_day = {}
        for calendar, events in calendar_events.items():
            for event in events:
                event_date = event.start if event.is_all_day else event.start.astimezone().date()
                if event_date not in events_by_day:
                    events_by_day[event_date] = {}
                    events_by_day = dict(sorted(events_by_day.items()))
                if calendar not in events_by_day[event_date]:
                    events_by_day[event_date][calendar] = []
                events_by_day[event_date][calendar].append(event)
        return events_by_day

    def generate_message(self, calendar_events: dict[str, list[CalendarItem]], days: int, check_date: date) -> str:
        if not any(calendar_events.values()):
            if days > 1 and check_date == date.today():
                return messages.NO_EVENTS_NEXT_DAYS.format(days=days)
            elif days > 1 and check_date != date.today():
                return messages.NO_EVENTS_DATE_DAYS.format(days=days, date=check_date)
            elif days <= 1 and check_date == date.today():
                return messages.NO_EVENTS_TODAY
            elif days <= 1 and check_date != date.today():
                return messages.NO_EVENTS_DATE.format(date=check_date)
            else:
                raise ValueError(f'Got an unhandled state: days = "{days}", check_date = "{check_date}"')

        events_by_day = self._sort_events_by_day(calendar_events)

        message_template = self.templates['message']
        if days > 1:
            if check_date == date.today():
                message_text = [messages.EVENTS_NEXT_DAYS.format(days=days)]
            else:
                message_text = [messages.EVENTS_DATE_DAYS.format(days=days, date=check_date)]
            for day, calendar_events in events_by_day.items():
                message_text.append(f'### {day}')
                message_text.append(message_template.render(calendar_events=calendar_events))
        else:
            if check_date == date.today():
                message_text = [messages.EVENTS_TODAY]
            else:
                message_text = [messages.EVENTS_DATE.format(date=check_date)]
            for calendar_events in events_by_day.values():
                message_text.append(message_template.render(calendar_events=calendar_events))

        return '\n'.join(message_text)

    async def check_manager(self, user_id: UserID) -> bool:
        for management_room in self.config['management_rooms']:
            managers = await self.client.get_joined_members(RoomID(management_room))
            return user_id in managers
        return False

    @command.new(help='Check reservations for days from optional date')
    @command.argument('days', 'Days', required=True, parser=int)
    @command.argument('check_date', 'Date', required=False, parser=_parse_date)
    async def check(self, evt: MessageEvent, days: int, check_date: date | None) -> None:
        self.log.debug('Checking reservations for %s days from %s', days, check_date)

        if not await self.check_manager(evt.sender) and evt.room_id not in self.config['allowlist']:
            await evt.reply('Sorry, you are not allowed to do that.')
            return

        if check_date is None:
            await self._send_message(room_id=evt.room_id, days=days, reply_to_event=evt.event_id)
        else:
            await self._send_message(room_id=evt.room_id, days=days, check_date=check_date, reply_to_event=evt.event_id)
