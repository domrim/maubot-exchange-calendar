NO_EVENTS_DATE = 'Am {date} gibt es keine Büro-Reservierungen.'
NO_EVENTS_DATE_DAYS = 'In den {days} Tagen ab {date} gibt es keine Büro-Reservierungen.'

NO_EVENTS_TODAY = 'Heute gibt es keine Büro-Reservierungen. Habt einen schönen Tag!'
NO_EVENTS_NEXT_DAYS = 'Für die nächsten {days} Tage gibt es keine Büro-Reservierungen. Habt einen schönen Tag!'

EVENTS_DATE = 'Am {date} gibt es die folgenden Büro-Reservierungen:'
EVENTS_DATE_DAYS = 'In den {days} Tagen ab {date} gibt es die folgenden Büro-Reservierungen:'

EVENTS_TODAY = 'Heute gibt es die folgenden Büro-Reservierungen:'
EVENTS_NEXT_DAYS = 'Für die nächsten {days} Tage gibt es die folgenden Büro-Reservierungen:'
